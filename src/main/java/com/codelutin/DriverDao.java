package com.codelutin;

import com.codelutin.beans.Driver;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class DriverDao {

    public Integer save(Driver driver) {
        EntityManager em = JpaUtil.getEntityManager();
        em.getTransaction().begin();
        em.persist(driver);
        em.getTransaction().commit();
        em.close();
        return driver.getId();
    }

    public Driver load(Integer id) {
        EntityManager em = JpaUtil.getEntityManager();
        Driver driver = em.find(Driver.class, id);
        em.close();
        return driver;
    }

    public List<Driver> getAll() {
        EntityManager em = JpaUtil.getEntityManager();
        em.getTransaction().begin();
        String select = " from " + Driver.class.getName() + " d";

        List<Driver> drivers = em.createQuery(select, Driver.class).getResultList();
        em.close();
        return drivers;
    }
}
