package com.codelutin;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class JpaUtil {

	private static EntityManagerFactory entityManagerFactory;

	static {
        entityManagerFactory = Persistence.createEntityManagerFactory("com.codelutin.formationjava.jpa");
    }
    public static EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    public static void closeFactory() {
        entityManagerFactory.close();
    }
}
