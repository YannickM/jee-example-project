package com.codelutin.beans;

import com.codelutin.exceptions.NotEnoughFuelException;
import com.codelutin.exceptions.TooMuchRefillException;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class Vehicle implements Serializable {

    private static final long serialVersionUID = 5672185212584143732L;

    protected String model;
    protected int year;
    protected float consumption;
    protected int fuelTankCapacity;
    protected float fuel;
    protected Set<Journey> journeys;
    protected Set<Refill> refills;

    public Vehicle(String model, int year, float consumption, int fuelTankCapacity) {
        this.model = model;
        this.year = year;
        this.consumption = consumption;
        this.fuelTankCapacity = fuelTankCapacity;
        this.fuel = fuelTankCapacity;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public float getConsumption() {
        return consumption;
    }

    public void setConsumption(float consumption) {
        this.consumption = consumption;
    }

    public int getFuelTankCapacity() {
        return fuelTankCapacity;
    }

    public void setFuelTankCapacity(int fuelTankCapacity) {
        this.fuelTankCapacity = fuelTankCapacity;
    }

    public float getFuel() {
        return fuel;
    }

    public void setFuel(float fuel) {
        this.fuel = fuel;
    }


    //TODO ymartel 20180118 move this logic to a service
    public void addJourney(Journey journey) throws NotEnoughFuelException {
        float fuelConsumption = journey.getDistance() * consumption / 100;
        if (this.fuel < fuelConsumption) {
            throw new NotEnoughFuelException(this.fuel * this.consumption, "Not enough fuel to make this journey");
        }
        if (this.journeys == null) {
            this.journeys = new HashSet<Journey>();
        }
        this.journeys.add(journey);
        this.fuel -= fuelConsumption;
    }

    //TODO ymartel 20180118 move this logic to a service
    public void refill(float fuel, Driver driver) throws TooMuchRefillException {
        if (this.fuel + fuel > fuelTankCapacity) {
            throw new TooMuchRefillException("Cannot add so much full, we already have : " + this.fuel + "/" + this.fuelTankCapacity);
        }
        Refill refill = new Refill(fuel, driver);
        if (this.refills == null) {
            this.refills = new HashSet<>();
        }
        this.refills.add(refill);
        this.fuel += fuel;
    }

}
