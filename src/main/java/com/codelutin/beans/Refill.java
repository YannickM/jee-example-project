package com.codelutin.beans;

import java.io.Serializable;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class Refill implements Serializable {

    private static final long serialVersionUID = -6855127143351565831L;

    protected float fuel;
    protected Driver who;

    public Refill(float fuel, Driver who) {
        this.fuel = fuel;
        this.who = who;
    }

    public Driver getWho() {
        return who;
    }

    public void setWho(Driver who) {
        this.who = who;
    }
}
