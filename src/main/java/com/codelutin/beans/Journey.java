package com.codelutin.beans;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class Journey implements Serializable {

    private static final long serialVersionUID = 2058449003528545276L;

    protected int distance;

    protected Set<Driver> drivers;

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public Set getDrivers() {
        return drivers;
    }

    public void addDriver(Driver driver) {
        if (this.drivers == null) {
            this.drivers = new HashSet<Driver>();
        }
        this.drivers.add(driver);
    }

    public String display() {
        StringBuffer stringBuffer = new StringBuffer(distance + " km parcourru, par :");
        for (Driver driver : drivers) {
            stringBuffer.append(" " + driver.getName());
        }
        return stringBuffer.toString();
    }
}
