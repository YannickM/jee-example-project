package com.codelutin.exceptions;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class TooMuchRefillException extends Exception {

    private static final long serialVersionUID = 5088710519532910500L;

    public TooMuchRefillException(String message) {
        super(message);
    }
}
